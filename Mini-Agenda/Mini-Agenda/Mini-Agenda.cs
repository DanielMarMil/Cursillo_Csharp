﻿using System;
using System.Collections;

namespace Mini_Agenda
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable agenda = new Hashtable();
            int key = 0;
            Persona [] persona = new Persona[100];
            Console.WriteLine("Programa de Mini-Agenda utilizando Hastable para crear la agenda");
            Console.WriteLine("Y una structura Persona donde guardar los datos: Nombre - Apellidos - Telefono");
            Console.WriteLine("Esta aplicación no esta preparada para la introducción erroneas de datos");
            Console.WriteLine("Acciones: Agregar Persona 'ag' - Buscar Persona mediante nombre 'bpn'");
            Console.WriteLine("Buscar Persona mediante el telefono 'bpt' - Mostrar Agenda completa 'all'");
            Console.WriteLine("Salir 'exit'");
            // Inicializando agenda, para que tenga algo que mostrar si se hace un all
            persona[key] = new Persona();
            persona[key].Nombre = "Nikola";
            persona[key].Apellidos = "Tesla";
            persona[key].Tlfn = 123654798;
            agenda.Add(key, persona[key]);
            key++;
            persona[key] = new Persona();
            persona[key].Nombre = "Richard";
            persona[key].Apellidos = "Stallman";
            persona[key].Tlfn = 010101010;
            agenda.Add(key, persona[key]);
            key++;
            persona[key] = new Persona();
            persona[key].Nombre = "Linus";
            persona[key].Apellidos = "Torvald";
            persona[key].Tlfn = 101010101;
            agenda.Add(key, persona[key]);
            key++;
            string accion = "";
            while (!accion.Equals("exit"))
            {
                Console.WriteLine("Accion: ");
                accion = Console.ReadLine();
                accion = accion.ToLower();
                switch (accion)
                {
                    case "ag":
                        persona[key] = new Persona();
                        Console.WriteLine("Nombre: ");
                        persona[key].Nombre = Console.ReadLine();
                        Console.WriteLine("Apellidos: ");
                        persona[key].Apellidos = Console.ReadLine();
                        Console.WriteLine("Telefono");
                        persona[key].Tlfn = int.Parse(Console.ReadLine());
                        agenda.Add(key, persona[key]);
                        key++;
                        break;
                    case "bpn":
                        Console.WriteLine("Nombre: ");
                        string aux = Console.ReadLine();
                        aux = aux.ToLower();
                        Persona p = new Persona();
                        foreach (DictionaryEntry contenido in agenda)
                        {
                            p = (Persona)contenido.Value;
                            if (aux.Equals(p.Nombre.ToLower()))
                            {
                                Console.WriteLine(contenido.Value.ToString());
                            }
                        }
                        break;
                    case "bpt":
                        Console.WriteLine("Telefono: ");
                        int auxint = int.Parse(Console.ReadLine());
                        Persona p2 = new Persona();
                        foreach (DictionaryEntry contenido in agenda)
                        {
                            p = (Persona)contenido.Value;
                            if (auxint.Equals(p.Tlfn))
                            {
                                Console.WriteLine(contenido.Value.ToString());
                            }
                        }
                        break;
                    case "all":
                        Console.WriteLine("Mostrando la agenda Completa");
                        foreach (DictionaryEntry contenido in agenda)
                        {
                            Console.WriteLine(contenido.Value.ToString());
                        }
                        break;
                    default:
                        Console.WriteLine("Acciones: Agregar Persona 'ag' - Buscar Persona mediante nombre 'bpn'");
                        Console.WriteLine("Buscar Persona mediante el telefono 'bpt' - Mostrar Agenda completa 'all'");
                        Console.WriteLine("Salir 'exit'");
                        break;
                }
            }

        }
    }
    class Persona
    {
        public String Nombre { get; set; }
        public String Apellidos { get; set; }
        public int Tlfn { get; set; }
        public Persona()
        {
            Nombre = "";
            Apellidos = "";
            Tlfn = 000000000;
        }
        public override string ToString()
        {
            return " Nombre - "+Nombre + " Apellidos - "+Apellidos + " Teléfono - "+ Tlfn;
        }
    }
}