﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora
{
    class Calculadora
    {
        static bool EsNumero(object String)
        {
            bool esNumero;
            int numero;
            esNumero = int.TryParse(Convert.ToString(String), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out numero);
            return esNumero;
        }
        static int[] Valores(int[] val)
        {
            string lectura;
            Console.WriteLine("Introduzca operando 1: ");
            lectura = Console.ReadLine();
            if (!EsNumero(lectura))
            {
                Console.WriteLine("Error: No ha introducido un valor numerico");
                Valores(val);
            }
            val[0] = Convert.ToInt32(lectura);
            Console.WriteLine("Introduzca operando 2: ");
            lectura = Console.ReadLine();
            if (!EsNumero(lectura))
            {
                Console.WriteLine("Error: No ha introducido un valor numerico");
                Valores(val);
            }
            val[1] = Convert.ToInt32(lectura);
            return val;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Cálculadora simple \n Esta cálculadora sólo operará operaciones simples");
            Console.WriteLine("Suma + Resta - Multiplicación * División /");
            String operando = null;
            int resultado;
            int[] v = new int[2];
            do
            {
                Console.WriteLine("Operando: ");
                operando = Console.ReadLine();
                switch (operando)
                {
                    case "+":
                        Valores(v);
                        resultado = v[0] + v[1];
                        Console.WriteLine("Resultado: {0}", resultado);
                        break;
                    case "-":
                        Valores(v);
                        resultado = v[0] - v[1];
                        Console.WriteLine("Resultado: {0}", resultado);
                        break;
                    case "*":
                        Valores(v);
                        resultado = v[0] * v[1];
                        Console.WriteLine("Resultado: {0}", resultado);
                        break;
                    case "/":
                        Valores(v);
                        resultado = v[0] / v[1];
                        Console.WriteLine("Resultado: {0}", resultado);
                        break;
                    default:
                        Console.WriteLine("Seleccione una operación correcta '+' '-' '*' '/' ");
                        break;
                }

            } while (!operando.Equals("exit") && !operando.Equals("Exit") && !operando.Equals("EXIT"));
        }
    }
}
