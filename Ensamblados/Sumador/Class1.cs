﻿namespace Sumador
{
    public class Fun_Basic
    {
        public int suma(int a, int b)
        {
            return a + b;
        }
        public int resta(int a, int b)
        {
            return a - b;
        }
        public int sucesor(int a)
        {
            return a + 1;
        }
        public int antecesor(int a)
        {
            return a - 1;
        }
        public double raiz(int a)
        {
            return System.Math.Sqrt(a);
        }
        public int exponente(int a, int b)
        {
            return a ^ b;
        }
        public int exponente(int a)
        {
            return a ^ 2;
        }
    }
}
