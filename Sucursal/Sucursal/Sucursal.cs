﻿using System;

namespace Sucursal
{
    class Sucursal
    {
        public class Banco
        {
            int NCTAS = 10;
            int NUMCTALIBRE = 1001;
            String nombre;
            int ppl=0;
            int unca = 1001;
            Cuenta[] cuentas;

            public Banco(string nombre)
            {
                this.nombre = nombre;
                cuentas = new Cuenta[NCTAS];
            }
            public Banco(string nombre, int numero)
            {
                this.nombre = nombre;
                this.NCTAS = numero;
                cuentas = new Cuenta[NCTAS];
            }
            private Cuenta [] AmpliarCuentas()
            {
                Cuenta[] cuentas_new = new Cuenta[NCTAS];
                for (int i = 0; i < ppl; i++)
                {
                    cuentas_new[i] = cuentas[i];
                }
                return cuentas_new;
            }
            public int AbrirCuenta(string titular, double saldo)
            {
                if (ppl == NCTAS)
                {
                    this.NCTAS *= 2;
                    this.cuentas=AmpliarCuentas();
                }
                cuentas[ppl] = new Cuenta(titular, unca, saldo);
                this.ppl++;
                unca++;
                return unca - 1;
            }
            public int AbrirCuenta(string titular)
            {
                return AbrirCuenta(titular,0.0);
            }
            private int PosicionCuenta(int cuenta)
            {
                int num_cuenta=0;
                while (cuenta!=cuentas[num_cuenta].GetCuenta())
                {
                    num_cuenta++;
                }
                return num_cuenta;
            }
            public void CerrarCuenta(int cuenta)
            {
                NUMCTALIBRE++;
                for (int i = PosicionCuenta(cuenta); i < ppl; i++)
                {
                    this.cuentas[i] = this.cuentas[i + 1];
                }
                this.ppl--;
            }
            public void Ingreso(int cuenta, double saldo)
            {
                cuentas[PosicionCuenta(cuenta)].Ingreso(saldo);
            }
            public double Debito(int cuenta, double saldo)
            {
                double cantidad = 0.0;
                int direccion = PosicionCuenta(cuenta);
                if (cuentas[direccion].Saldo() >= saldo)
                {
                    cantidad = saldo;
                    cuentas[direccion].Debito(saldo);
                }
                else
                {
                    cantidad = cuentas[direccion].Saldo();
                    cuentas[direccion].Debito(cantidad);
                }
                return cantidad;
            }
            public double Transferencia(int org, int dest, double saldo)
            {
                double traspaso = Debito(org, saldo);
                Ingreso(dest, traspaso);
                return traspaso;
            }
            public override string ToString()
            {
                string mostrar = "Sucursal: " + nombre + "\n";
                for (int i=0; i < ppl; i++)
                {
                        mostrar = mostrar + cuentas[i].ToString();
                }
                return mostrar;
            }
            public double Saldo(int cuenta)
            {
                return cuentas[PosicionCuenta(cuenta)].Saldo();
            }
        }
        public class Cuenta
        {
            String titular;
            double saldo;
            int cuenta;

            public Cuenta(string titular, int cuenta, double saldo)
            {
                this.titular = titular;
                this.saldo = saldo;
                this.cuenta = cuenta;
            }
            public Cuenta(string titular, int cuenta)
            {
                this.titular = titular;
                this.cuenta = cuenta;
                this.saldo = 0.0;
            }
            public void Ingreso(double ingreso)
            {
                saldo += ingreso;
            }
            public void Debito(double debito)
            {
                if (debito <= saldo)
                {
                    saldo -= debito;
                }
            }
            public string Titular()
            {
                return titular;
            }
            public double Saldo()
            {
                return saldo;
            }
            public int GetCuenta()
            {
                return cuenta;
            }
            public new string ToString()
            {
                string prueba = " -> Titular: "+Titular()+" - Saldo: "+Saldo()+" - Numero de cuenta: "+GetCuenta() + "\n";
                return prueba;
            }
        }
        static void Main(string[] args)
        {
            Banco b = new Banco("Santander",100);
            b.AbrirCuenta("Clara", 200.0);
            b.AbrirCuenta("Sebastian", 150.0);
            b.AbrirCuenta("Cifuentes",1000000000000.0);
            string operando="exit",titular;
            string s1=null, s2=null;
            bool salir = false;
            Console.WriteLine("Operando: + añadir cuenta, - quitar una cuenta, t transferencia de una cuenta a la otra\n");
            Console.WriteLine("i hacer un ingreso a una cuenta, d hacer un debito a una cuenta, all ver todas las cuentas del banco.\n");
            Console.WriteLine("Para salir, introducir exit, todos los valores pedidos en los folmularios deben de ser los correctos, no admite errores.\n");
            do
            {
                operando = Console.ReadLine();
                operando=operando.ToLower();
                switch (operando)
                {
                    case "+":
                        Console.WriteLine("Titular de la cuenta:");
                        titular = Console.ReadLine();
                        Console.WriteLine("Saldo inicial de la cuenta, en caso de no ingresar valor por defecto será 0.0:");
                        s1 = Console.ReadLine();
                        if (s1=="")
                        {
                            b.AbrirCuenta(titular);
                        }
                        else
                        {
                            b.AbrirCuenta(titular, Double.Parse(s1));
                        }
                        Console.WriteLine("Cuenta añadida correctamente");
                        break;
                    case "-":
                        Console.WriteLine("Numero de cuenta que desea eliminar, si no recuerda cual es, no introduzca nada y seleccione la opcion de all:");
                        titular = Console.ReadLine();
                        if (titular != null)
                        {
                            b.CerrarCuenta(int.Parse(titular));
                        }
                        Console.WriteLine("Cuenta cerrada correctamente");
                        break;
                    case "t":
                        Console.WriteLine("Número de cuenta origen:");
                        titular = Console.ReadLine();
                        Console.WriteLine("Dinero que desea traspasar:");
                        s1 = Console.ReadLine();
                        Console.WriteLine("Número de cuenta destino:");
                        s2 = Console.ReadLine();
                        Console.WriteLine("Se ha llegado a traspasar: "+b.Transferencia(int.Parse(titular),int.Parse(s2),double.Parse(s1)));
                        break;
                    case "i":
                        Console.WriteLine("Introducir el número de cuenta al que quiera hacer el ingreso:");
                        titular = Console.ReadLine();
                        Console.WriteLine("Saldo que quiera ingresar en la cuenta:");
                        s1 = Console.ReadLine();
                        b.Ingreso(int.Parse(titular),double.Parse(s1));
                        Console.WriteLine("Cuenta ingresado la cantidad correctamente");
                        break;
                    case "d":
                        Console.WriteLine("Introducir el número de cuenta al que quiera hacer el debito:");
                        titular = Console.ReadLine();
                        Console.WriteLine("Saldo que quiera retirar en la cuenta:");
                        s1 = Console.ReadLine();
                        Console.WriteLine("Se ha llegado a realizar un debito de : " + b.Debito(int.Parse(titular), double.Parse(s1)));
                        break;
                    case "all":
                        Console.WriteLine(b.ToString());
                        break;
                    case "exit":
                        salir = true;
                        break;
                    default:
                        Console.WriteLine("Seleccione una operación correcta '+' '-' 'i' 't' 'all' 'd' 'exit'");
                        break;
                }
            } while (!salir);
        }
    }
}